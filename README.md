# 20230315-AryehBaker-NYCSchools

## Description
An app that gets general school data and school SAT averages and puts the data in a recyclerview. 
Initially, the SAT data for the school is hidden. 
When pressing the down arrow for a school then the SAT data shows under the school data 


## Architecture:
The activity sets up the RecyclerView UI and connects it to the data captured by the ViewModel.
The ViewModel has the data prepared for the UI from GetSchoolData.
The data layer is in APIs folder and uses standard callbacks.
The pojos are in DataTypes folder.


## Todo:
- error handling - putting states in the ViewModel that would lead to correct state on screen.
- Research what a user wants to see on the main card and when opened up to details.
  An example is percent who took the sat compared to results and how many other fields.
  I imagine showing all fields available in an auto styled way in a big TextView may make sense.


## Nice to have:
- pretty up interface
- more unit testing
- instrumented tests
- use RxJava to make a cleaner description of the flow in code - there are tradeoffs though
- make API calls at the same time
- change from singleton to Application class for retrofit class setup
- set up a paging service for the API's to update recycler view for slow networks
- Show error in UI with button to retry if applicable.
- make good layout for smaller screens
- data binding for simplicity of setting text
- make display pretty
