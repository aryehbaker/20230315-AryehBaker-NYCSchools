package com.example.schooldata;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.schooldata.DataTypes.ItemCombinedData;
import com.example.schooldata.DataTypes.School;
import com.example.schooldata.DataTypes.SchoolAverage;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.RuntimeEnvironment;
import java.util.ArrayList;
import java.util.List;
@RunWith(RobolectricTestRunner.class)
public class SchoolAdapterTest {
    SchoolAdapter adapter;
    RecyclerView recyclerView;

    @Before
    public void setUp()  {

        adapter = new SchoolAdapter(RuntimeEnvironment.getApplication());
        recyclerView = new RecyclerView(RuntimeEnvironment.getApplication());
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(RuntimeEnvironment.getApplication()));

    }

        @Test
        public void itemCount()  {
            List<ItemCombinedData> data = new ArrayList<>();
            data.add(new ItemCombinedData(new School(),new SchoolAverage()));
            data.add(new ItemCombinedData(new School(),new SchoolAverage()));
            data.add(new ItemCombinedData(new School(),new SchoolAverage()));
            adapter.updateData(data);
            Assert.assertEquals(3, adapter.getItemCount());
        }
}