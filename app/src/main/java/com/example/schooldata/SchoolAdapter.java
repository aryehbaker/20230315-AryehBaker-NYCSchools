package com.example.schooldata;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.example.schooldata.DataTypes.ItemCombinedData;
import com.example.schooldata.DataTypes.School;
import com.example.schooldata.DataTypes.SchoolAverage;
import java.util.List;
/**
 * SchoolAdapter shows combined data from schools and their SAT scores.
 * SAT scores are initially hidden until the down button is pressed.
 * When pressing again sat scores become hidden
 */
public class SchoolAdapter extends RecyclerView.Adapter<SchoolAdapter.ViewHolder> {
    private List<ItemCombinedData> itemCombinedDataList;
    private final Context context;
    public SchoolAdapter(Context context) {
        this.context = context;
    }
    /**
     * @param data - the data to update the recyclerview
     */
    public void updateData(List<ItemCombinedData> data){
        this.itemCombinedDataList = data;
        notifyDataSetChanged();
    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.school_item, parent, false);
        return new ViewHolder(view);
    }
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        ItemCombinedData data = itemCombinedDataList.get(position);
        School school = data.school;
        SchoolAverage avg = data.schoolAverage;
        holder.schoolNameTextView.setText(school.getSchoolName());
        holder.primaryAddressLine1TextView.setText(school.getPrimaryAddressLine1());
        holder.cityTextView.setText(school.getCity());
        holder.stateCodeTextView.setText(school.getStateCode());
        holder.zipTextView.setText(school.getZip());
        holder.totalStudentsTextView.setText(school.getTotalStudents());
        holder.testakersTextView.setText(avg.getNumOfSatTestTakers());
        holder.readingTextView.setText(avg.getSatCriticalReadingAvgScore());
        holder.writingTextView.setText(avg.getSatWritingAvgScore());
        holder.mathTextView.setText(avg.getSatMathAvgScore());
    }
    @Override
    public int getItemCount() {
          if(itemCombinedDataList == null)return 0;
        return itemCombinedDataList.size();
    }
   public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView schoolNameTextView;
        public TextView primaryAddressLine1TextView;
        public TextView cityTextView;
        public TextView stateCodeTextView;
        public TextView zipTextView;
        public TextView totalStudentsTextView;
        public TextView testakersTextView;
        TextView testakersLabel;
        public TextView readingTextView;
        TextView readingLabel;
        public TextView writingTextView;
        TextView writingLabel;
        public TextView mathTextView;
        TextView mathLabel;
        public ImageButton arrow;
        private boolean isExpanded = false;
        /**
         * Click Listener expands/contracts the itemview
         * clicking the down button reveals SAT scores and sets the button to up. Clicking the up button
         * hides SAT scores and sets the button to down
         * {@code isExpanded}  keeps track of state of expansion
         */
        public ViewHolder(View itemView) {
            super(itemView);
            schoolNameTextView = itemView.findViewById(R.id.school_name);
            primaryAddressLine1TextView = itemView.findViewById(R.id.primary_address_line_1);
            cityTextView = itemView.findViewById(R.id.city);
            stateCodeTextView = itemView.findViewById(R.id.state_code);
            zipTextView = itemView.findViewById(R.id.zip);
            totalStudentsTextView = itemView.findViewById(R.id.total_students);
            testakersLabel = itemView.findViewById(R.id.testTakersLabel);
            testakersTextView = itemView.findViewById(R.id.testTakers);
            readingLabel = itemView.findViewById(R.id.criticalReadingLabel);
            readingTextView = itemView.findViewById(R.id.reading);
            writingLabel = itemView.findViewById(R.id.criticalWritingLabel);
            writingTextView = itemView.findViewById(R.id.writing);
            mathLabel = itemView.findViewById(R.id.criticalMathLabel);
            mathTextView = itemView.findViewById(R.id.math);
            arrow = itemView.findViewById(R.id.itemAdjust);
 //todo: replace the following hider/shower with a sublayout and hiding/showing the sublayout
            arrow.setOnClickListener(view -> {
                if (isExpanded) {
                    testakersLabel.setVisibility(View.GONE);
                    testakersTextView.setVisibility(View.GONE);
                    readingLabel.setVisibility(View.GONE);
                    readingTextView.setVisibility(View.GONE);
                    writingLabel.setVisibility(View.GONE);
                    writingTextView.setVisibility(View.GONE);
                    mathLabel.setVisibility(View.GONE);
                    mathTextView.setVisibility(View.GONE);
                    arrow.setImageDrawable(context.getDrawable(R.drawable.ic_action_down));
                    isExpanded = false;
                } else {
                    testakersLabel.setVisibility(View.VISIBLE);
                    testakersTextView.setVisibility(View.VISIBLE);
                    readingLabel.setVisibility(View.VISIBLE);
                    readingTextView.setVisibility(View.VISIBLE);
                    writingLabel.setVisibility(View.VISIBLE);
                    writingTextView.setVisibility(View.VISIBLE);
                    mathLabel.setVisibility(View.VISIBLE);
                    mathTextView.setVisibility(View.VISIBLE);
                    arrow.setImageDrawable(context.getDrawable(R.drawable.ic_action_up));
                    isExpanded = true;
                }
            });
        }
    }
}
