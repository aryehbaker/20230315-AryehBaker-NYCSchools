package com.example.schooldata;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;
import com.example.schooldata.APIs.GetSchoolData;
import com.example.schooldata.DataTypes.ItemCombinedData;
import com.example.schooldata.APIs.Result;
import java.util.List;
public class MainActivityVM extends ViewModel {
    private final MutableLiveData<List<ItemCombinedData>> liveCombinedDataSet;
    /**
     * the dataset of all schools with averages sent to recyclerview
     */
    public LiveData<List<ItemCombinedData>> recyclerData;
    public MainActivityVM() {
        liveCombinedDataSet = new MutableLiveData<>();
        recyclerData = liveCombinedDataSet;
        GetSchoolData getSchoolData = new GetSchoolData();
        getSchoolData.getSchoolData(result -> {
           if (result instanceof Result.Success) {
               // Happy path
               liveCombinedDataSet.setValue(((Result.Success<List<ItemCombinedData>>) result).data);
           } else {
               // todo; Show error in UI with button to retry if applicable
           }
       });
    }
}

