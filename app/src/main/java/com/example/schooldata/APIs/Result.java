package com.example.schooldata.APIs;
/**
 * @param <T> is any result return type, the extended types 'Success' and the data is returned,
 *           or an 'Error' that can be returned
 */
public abstract class Result<T> {
    private Result() {}
    public static final class Success<T> extends Result<T> {
        public T data;
        /**
         * @param data is the abstract data returned by function
         */
        public Success(T data) {
            this.data = data;
        }
    }
    public static final class Error<T> extends Result<T> {
        public Exception exception;
        public Error(Exception exception) {
            this.exception = exception;
        }
    }
}
