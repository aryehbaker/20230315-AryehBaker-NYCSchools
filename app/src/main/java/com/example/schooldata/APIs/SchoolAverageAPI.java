package com.example.schooldata.APIs;

import com.example.schooldata.DataTypes.SchoolAverage;
import java.util.List;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;
/**
 * gets all school averages or one
 * todo; use the get one average for slower networks
*/
public interface SchoolAverageAPI {
    /**
     * function getSchoolAverages()
     * @return List<SchoolAverage>  {@link com.example.schooldata.DataTypes.SchoolAverage }
     * @see <a href="https://data.cityofnewyork.us/Education/2012-SAT-Results/f9bf-2cp4">API fields descriptions</a> for general API info
     * @see <a href="https://dev.socrata.com/docs/filtering.html">API instructions</a> for API functions
     **/
    @GET("resource/f9bf-2cp4.json?$select=dbn,num_of_sat_test_takers,sat_critical_reading_avg_score,sat_math_avg_score,sat_writing_avg_score")
    Call<List<SchoolAverage>> getSchoolAverages();
    @GET("resource/f9bf-2cp4.json?$select=dbn,num_of_sat_test_takers,sat_critical_reading_avg_score,sat_math_avg_score,sat_writing_avg_score")
    Call<List<SchoolAverage>> getSchoolAverage(@Query("dbn") String dbn);
}
