package com.example.schooldata.APIs;
public interface RepositoryCallback<T> {
    void onComplete(Result<T> result);
}