package com.example.schooldata.APIs;

import com.example.schooldata.DataTypes.School;
import java.util.List;
import retrofit2.Call;
import retrofit2.http.GET;
/**
 * gets all school general information
 * todo: get pages for slow network
 */
public interface SchoolInfoAPI {
    /**
     * function getSchools() - retrofit API setup to get Schools information
     * @return List<School>  {@link com.example.schooldata.DataTypes.School }
     * @see <a href="https://data.cityofnewyork.us/Education/2017-DOE-High-School-Directory/s3k6-pzi2">API fields descriptions</a> for general API info
     * @see <a href="https://dev.socrata.com/docs/filtering.html">API instructions</a> for API functions
     */
    @GET("resource/s3k6-pzi2.json?$select=dbn,school_name,total_students,primary_address_line_1,city,state_code,zip")
    Call<List<School>> getSchools();
}
