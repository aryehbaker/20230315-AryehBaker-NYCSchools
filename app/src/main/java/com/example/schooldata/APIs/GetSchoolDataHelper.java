package com.example.schooldata.APIs;

import com.example.schooldata.DataTypes.ItemCombinedData;
import com.example.schooldata.DataTypes.School;
import com.example.schooldata.DataTypes.SchoolAverage;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * provides helper functions for {@link com.example.schooldata.APIs.GetSchoolData}
 * which makes GetSchoolData easier to read
 */
public class GetSchoolDataHelper {
    List<ItemCombinedData> combinedData;
//todo: change from singleton to Application class for retrofit class setup
    private static GetSchoolDataHelper instance;
    public static GetSchoolDataHelper getRetrofitNYC (){
        if (instance == null){
            instance = new GetSchoolDataHelper();
        }
        return instance;
    }
     private final Retrofit retrofit = new Retrofit.Builder()
            .baseUrl("https://data.cityofnewyork.us/")
            .addConverterFactory(GsonConverterFactory.create())
            .build();
    SchoolInfoAPI schoolInfoAPI = retrofit.create(SchoolInfoAPI.class);
    SchoolAverageAPI schoolAverageAPI = retrofit.create(SchoolAverageAPI.class);
    /**
     * getSchools() pulls visible information on  itemView
     */
    public void getSchools(Callback<List<School>> callback) {
        Call<List<School>> call = schoolInfoAPI.getSchools();
        call.enqueue(callback);
    }
    /**
     * @param callback for either returning errors or successful call with result
     */
    public void getSchoolAverages(Callback<List<SchoolAverage>> callback) {
        Call<List<SchoolAverage>> call = schoolAverageAPI.getSchoolAverages();
        call.enqueue(callback);
    }
    /**
     * combineData() combines school data and school averages and then posts the result to a
     * livedata that is consumed by the recyclerview adapter. The adapter gets the data
     * from an observer in the MainActivity
     * @param schools - all school info
     * @param schoolsAverages all school average info
     * @return List<ItemCombinedData> a combined list of schools and school averages
     */
    public List<ItemCombinedData> combineData(List<School> schools, List<SchoolAverage> schoolsAverages) {
        combinedData = new ArrayList<>();
        for (int i = 0; i < schools.size(); i++) {
            for (int j = 0; j < schoolsAverages.size(); j++) {
                if (schools.get(i).getDbn().contentEquals(
                        schoolsAverages.get(j).getDbn())) {
                    combinedData.add(
                            new ItemCombinedData(
                                    schools.get(i)
                                    , schoolsAverages.get(j)
                            )
                    );
                }

            }

        }
        return combinedData;
    }

}
