package com.example.schooldata.APIs;

import com.example.schooldata.DataTypes.ItemCombinedData;
import com.example.schooldata.DataTypes.School;
import com.example.schooldata.DataTypes.SchoolAverage;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class GetSchoolData {
    /**
     * all schools information
     */
    List<School> schools = new ArrayList<>();//for combination of data
    /**
     * all school SAT averages information
     */
    List<SchoolAverage> schoolsAverages;//for combination of data
    /**
     * school and school averages information combined on field 'dbn'
     */
    List<ItemCombinedData> itemCombinedDataList; //what is returned
    GetSchoolDataHelper dataHelper;
        public void getSchoolData(RepositoryCallback<List<ItemCombinedData>> callback){
        dataHelper = GetSchoolDataHelper.getRetrofitNYC();
        getSchools(callback);
    }
        /*
         * todo; make calls at the same time
        * */
        /**
         * getSchools() starts the chain of calling for school information.
         * Automatically visible information is pulled in this call
         * Once Schools are downloaded then getSchoolAverages() for school averages  is called
         * @param callback returns errors and if successful goes on to getSchoolAverages()
         */
        private void getSchools(RepositoryCallback<List<ItemCombinedData>> callback) {
            dataHelper.getSchools(new Callback<List<School>>() {
                @Override
                public void onResponse(Call<List<School>> call, Response<List<School>> response) {
                    if (response.isSuccessful()) {
                        schools = response.body();
                        /*
                         * will add more error states that will auto populate with "no data"
                         * */
                        //next get the school averages using getSchoolAverages()
                        getSchoolAverages( callback);
                    } else {
                        callback.onComplete(new Result.Error(new Exception("no school data")));
                    }
                }

                @Override
                public void onFailure(Call<List<School>> call, Throwable t) {
                    callback.onComplete(new Result.Error((Exception) t));
                }
            });
        }

        /**
         * getSchoolAverages() gets school SAT averages information
         * Once School Averages are pulled then combineData() is called to
         * join general school data with school averages
         * @param callback returns errors and if successful returns the combined data
         */
        private void getSchoolAverages(RepositoryCallback<List<ItemCombinedData>> callback) {
            dataHelper.getSchoolAverages(new Callback<List<SchoolAverage>>() {
                @Override
                public void onResponse(Call<List<SchoolAverage>> call, Response<List<SchoolAverage>> response) {

                    if (response.isSuccessful()) {
                        schoolsAverages = response.body();
                        //Data is combined and recyclerView auto fills from Activity observer
                        itemCombinedDataList =
                                dataHelper.combineData(
                                        schools
                                        ,schoolsAverages
                                );


                        callback.onComplete(new Result.Success( itemCombinedDataList));
                    } else {
                        callback.onComplete(new Result.Error(new Exception("no school averages data")));
                    }
                }

                @Override
                public void onFailure(Call<List<SchoolAverage>> call, Throwable t) {
                    callback.onComplete(new Result.Error((Exception) t));
                }
            });
        }

    }



