package com.example.schooldata.DataTypes;
/**
 * a combined school and school average data using field 'dbn' as an index
 */
public class ItemCombinedData {
    public SchoolAverage schoolAverage;
    public School school;
    public ItemCombinedData(School school, SchoolAverage schoolAverage) {
        this.school = school;
        this.schoolAverage = schoolAverage;
    }
}