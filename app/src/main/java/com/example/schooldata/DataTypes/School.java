package com.example.schooldata.DataTypes;
import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
/**
 * an object from general school info api
 */
@Generated("jsonschema2pojo")
public class School {
    @SerializedName("dbn")
    @Expose
    private String dbn;
    @SerializedName("school_name")
    @Expose
    private String schoolName;
    @SerializedName("primary_address_line_1")
    @Expose
    private String primaryAddressLine1;
    @SerializedName("city")
    @Expose
    private String city;
    @SerializedName("state_code")
    @Expose
    private String stateCode;
    @SerializedName("zip")
    @Expose
    private String zip;
    @SerializedName("total_students")
    @Expose
    private String totalStudents;
    public String getDbn() {
        return dbn;
    }
    public void setDbn(String dbn) {
        this.dbn = dbn;
    }
    public String getSchoolName() {
        return schoolName;
    }
    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }
    public String getPrimaryAddressLine1() {
        return primaryAddressLine1;
    }
    public void setPrimaryAddressLine1(String primaryAddressLine1) {
        this.primaryAddressLine1 = primaryAddressLine1;
    }
    public String getCity() {
        return city;
    }
    public void setCity(String city) {
        this.city = city;
    }
    public String getStateCode() {
        return stateCode;
    }
    public void setStateCode(String stateCode) {
        this.stateCode = stateCode;
    }
    public String getZip() {
        return zip;
    }
    public void setZip(String zip) {
        this.zip = zip;
    }
    public String getTotalStudents() {
        return totalStudents;
    }
    public void setTotalStudents(String totalStudents) {
        this.totalStudents = totalStudents;
    }
}
